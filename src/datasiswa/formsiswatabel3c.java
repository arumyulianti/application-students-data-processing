/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasiswa;
import java.awt.Frame;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JSpinner.NumberEditor;
import koneksi.koneksi;
import javax.swing.*;
import javax.swing.table.*;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author ASUS
 */
public class formsiswatabel3c extends javax.swing.JFrame {
private Connection conn = new koneksi().connect();
private DefaultTableModel tabmode;
JasperDesign JasDes;
JasperReport JasRep;
JasperPrint JasPri;
Map param = new HashMap();
    /**
     * Creates new form formsiswatabel
     */
    public formsiswatabel3c() {
        
        initComponents();
        tampilmodel(); 
        datatable();
       setLocationRelativeTo(this);
    }

 
    
    public void removeTable(){
        try{
            for (int t=tabmode.getRowCount(); t>0; t--) {tabmode.removeRow(0);}
        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
   
    
    private void atur(JTable lihat,  int lebar[]){
    try{
        lihat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int banyak = lihat.getColumnCount();
        for (int i = 0; i > banyak; i++) {
           TableColumn kolom = lihat.getColumnModel().getColumn(i);
           kolom.setPreferredWidth(lebar[i]);
           lihat.setRowHeight(20);
        }
    } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "salah"+e);
    }
}
    
    private void tampilmodel(){
    try {
   String[]kolom={"NIS","Nama","Jenis Kelamin","NISN","Tempat Lahir","Tanggal Lahir","Bulan Lahir", "Tahun Lahir", "Usia", "Berat Badan", "Tinggi Badan", "NIK", "NO KK", "Agama", "Alamat", "RT", "RW","Kelurahan","Kecamatan","Kode Pos","Rombel","Sekolah Asal","Mutasi Ke","Nama Ayah","NIK Ayah","Tahun Lahir","Pendidikan","Pekerjaan","Penghasilan","No Telp","Nama Ibu","NIK Ibu","Tahun Lahir","Pendidikan","Pekerjaan","Penghasilan","No Telp"};
        
   DefaultTableModel dtm = new DefaultTableModel(null, kolom){            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };

         tabelsiswa.setModel(dtm);
            atur(tabelsiswa, new int []{100,300,300,90,90,90,90} );
       } catch (Exception e) {
         JOptionPane.showMessageDialog(null, "salah"+e);
    }
}

    
    protected void datatable(){
        
        int jumdata=0;
        Object[]Baris={"Nomor","Nama","Jenis Kelamin","NISN","Tempat Lahir","Tanggal Lahir","Bulan Lahir", "Tahun Lahir", "Usia", "Berat Badan", "Tinggi Badan", "NIK", "NO KK", "Agama", "Alamat", "RT", "RW","Kelurahan","Kecamatan","Kode Pos","Rombel","Sekolah Asal","Mutasi Ke","Nama Ayah","NIK Ayah","Tahun Lahir","Pendidikan","Pekerjaan","Penghasilan","No Telp","Nama Ibu","NIK Ibu","Tahun Lahir","Pendidikan","Pekerjaan","Penghasilan","No Telp"};
        tabmode = new DefaultTableModel(null,Baris);
        tabelsiswa.setModel(tabmode);
        String sql = "Select * from datakelas_3c";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
            String nomor = hasil.getString("nomor");
            String nama = hasil.getString("nama");
            String jeniskelamin = hasil.getString("jenis_kelamin");
            String nisn = hasil.getString("nisn");
            String tempat = hasil.getString("tempat");
            String tanggal = hasil.getString("tanggal");
            String bulan = hasil.getString("bulan");
            String tahun = hasil.getString("tahun");
            String usia = hasil.getString("usia");
            String beratbadan = hasil.getString("berat_badan");
            String tinggibadan = hasil.getString("tinggi_badan");
            String nik = hasil.getString("nik");
            String nokk = hasil.getString("no_kk");
            String agama = hasil.getString("agama");
            String alamat = hasil.getString("alamat");
            String rt = hasil.getString("rt");
            String rw = hasil.getString("rw");
            String kelurahan = hasil.getString("kelurahan");
            String kecamatan = hasil.getString("kecamatan");
            String kodepos = hasil.getString("kode_pos");
            String rombel = hasil.getString("rombel");
            String sekolahasal = hasil.getString("sekolah_asal");
            String mutasi = hasil.getString("mutasi");
            String namaayah = hasil.getString("nama_ayah");
            String nikayah = hasil.getString("nik_ayah");
            String tahunlahirayah = hasil.getString("tahun_lahir_ayah");
            String jenjangpendidikanayah = hasil.getString("jenjang_pendidikan_ayah");
            String pekerjaanayah = hasil.getString("pekerjaan_ayah");
            String penghasilanayah = hasil.getString("penghasilan_ayah");
            String notlpayah = hasil.getString("no_tlp_ayah");
            String namaibu = hasil.getString("nama_ibu");
            String nikibu = hasil.getString("nik_ibu");
            String tahunlahiribu = hasil.getString("tahun_lahir_ibu");
            String jenjangpendidikanibu = hasil.getString("jenjang_pendidikan_ibu");
            String pekerjaanibu = hasil.getString("pekerjaan_ibu");
            String penghasilanibu = hasil.getString("penghasilan_ibu");
            String notlpibu = hasil.getString("no_tlp_ibu");
            
            String[] data = {nomor,nama,jeniskelamin,nisn,tempat,tanggal,bulan,tahun,usia,beratbadan,tinggibadan,nik,nokk,agama,alamat,rt,rw,kelurahan,kecamatan,kodepos,rombel,sekolahasal,mutasi,namaayah,nikayah,tahunlahirayah,jenjangpendidikanayah,pekerjaanayah,penghasilanayah,notlpayah,namaibu,nikibu,tahunlahiribu,jenjangpendidikanibu,pekerjaanibu,penghasilanibu,notlpibu};
            tabmode.addRow(data);
            jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum.setText(jumdata+" Siswa");
        }

   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelsiswa = new javax.swing.JTable();
        cari = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lbljum = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        Bsadata = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 153, 0));

        jPanel7.setBackground(new java.awt.Color(51, 51, 51));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("DATA SISWA SDN GEDONG 10 PAGI");
        jPanel7.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(348, 0, -1, -1));

        jLabel34.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setText("Jl. H. Taiman Rt 005 / Rw 010 Kelurahan Gedong, Kecamatan Pasar Rebo");
        jPanel7.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, -1, -1));

        jLabel35.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setText("Jakarta Timur, DKI Jakarta");
        jPanel7.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 60, -1, -1));

        jLabel36.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setText("Kode Pos 13760");
        jPanel7.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(606, 83, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/osis sd.png"))); // NOI18N
        jPanel7.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 10, -1, -1));

        jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/tut wuri2.png"))); // NOI18N
        jPanel7.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        tabelsiswa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelsiswa);

        cari.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jButton1.setBackground(new java.awt.Color(204, 204, 0));
        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/search (1).png"))); // NOI18N
        jButton1.setText("CARI");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel2.setText("Masukkan NISN");

        lbljum.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        lbljum.setText("jLabel30");

        jButton5.setBackground(new java.awt.Color(204, 0, 0));
        jButton5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton5.setForeground(new java.awt.Color(255, 255, 255));
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/24 Pixel/005-back-arrow.png"))); // NOI18N
        jButton5.setText("MENU");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel31.setText("JUMLAH  DATA SISWA KELAS 3C :");

        jButton2.setBackground(new java.awt.Color(0, 51, 255));
        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("CETAK SELURUH DATA");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        Bsadata.setBackground(new java.awt.Color(0, 102, 51));
        Bsadata.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Bsadata.setForeground(new java.awt.Color(255, 255, 255));
        Bsadata.setText("CETAK DATA");
        Bsadata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BsadataActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 1290, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(68, 68, 68))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)
                        .addGap(18, 18, 18)
                        .addComponent(Bsadata, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbljum)
                        .addGap(71, 71, 71))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(63, 63, 63)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 135, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(lbljum)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Bsadata, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 640));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    String itemdicari = cari.getText();
    int jumdata=0;
    removeTable();
    try{
        Statement stat = conn.createStatement();
        String sql = "Select * from datakelas_3c where nisn like '%"+itemdicari+"%' or nama like '%"+itemdicari+"%' order by nisn asc";
        System.out.println(sql);
        
        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()){
          String nomor = rs.getString("nomor");
            String nama = rs.getString("nama");
            String jeniskelamin = rs.getString("jenis_kelamin");
            String nisn = rs.getString("nisn");
            String tempat = rs.getString("tempat");
            String tanggal = rs.getString("tanggal");
            String bulan = rs.getString("bulan");
            String tahun = rs.getString("tahun");
            String usia = rs.getString("usia");
            String beratbadan = rs.getString("berat_badan");
            String tinggibadan = rs.getString("tinggi_badan");
            String nik = rs.getString("nik");
            String kk = rs.getString("no_kk");
            String agama = rs.getString("agama");
            String alamat = rs.getString("alamat");
            String rt = rs.getString("rt");
            String rw = rs.getString("rw");
            String kelurahan = rs.getString("kelurahan");
            String kecamatan = rs.getString("kecamatan");
            String kodepos = rs.getString("kode_pos");
            String rombel = rs.getString("rombel");
            String sekolahasal = rs.getString("sekolah_asal");
            String mutasi = rs.getString("mutasi");
            String namaayah = rs.getString("nama_ayah");
            String nikayah = rs.getString("nik_ayah");
            String tahunlahirayah = rs.getString("tahun_lahir_ayah");
            String jenjangpendidikanayah = rs.getString("jenjang_pendidikan_ayah");
            String pekerjaanayah = rs.getString("pekerjaan_ayah");
            String penghasilanayah = rs.getString("penghasilan_ayah");
            String notlpayah = rs.getString("no_tlp_ayah");
            String namaibu = rs.getString("nama_ibu");
            String nikibu = rs.getString("nik_ibu");
            String tahunlahiribu = rs.getString("tahun_lahir_ibu");
            String jenjangpendidikanibu = rs.getString("jenjang_pendidikan_ibu");
            String pekerjaanibu = rs.getString("pekerjaan_ibu");
            String penghasilanibu = rs.getString("penghasilan_ibu");
            String notlpibu = rs.getString("no_tlp_ibu");
            
            String[] data = {nomor,nama,jeniskelamin,nisn,tempat,tanggal,bulan,tahun,usia,beratbadan,tinggibadan,nik,kk,agama,alamat,rt,rw,kelurahan,kecamatan,kodepos,rombel,sekolahasal,mutasi,namaayah,nikayah,tahunlahirayah,jenjangpendidikanayah,pekerjaanayah,penghasilanayah,notlpayah,namaibu,nikibu,tahunlahiribu,jenjangpendidikanibu,pekerjaanibu,penghasilanibu,notlpibu};
            tabmode.addRow(data);
            jumdata = jumdata++;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum.setText(jumdata + " Siswa");
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
    new formsiswaubahdata3c().show();
        this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    String reportsource = null;
    String reportdest = null;
        try{
            Connection conn = new koneksi().connect();
            reportsource = System.getProperty("user.dir")+"/src/report/reportkelas3csemua.jrxml";
            reportdest = System.getProperty("user.dir")+"/src/report/reportkelas3csemua.jasper";
            
            JasRep = JasperCompileManager.compileReport(reportsource);
            JasPri = JasperFillManager.fillReport(JasRep,param,conn);
            JasperViewer.viewReport(JasPri, false);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null," Report Gagal Di Print"+ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void BsadataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BsadataActionPerformed
    try{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/datasiswasdn10","root","");
        Statement statement = conn.createStatement();
        
            try{
                File reprt = new File("src/report/reportkelas3c.jrxml");
                JasDes = JRXmlLoader.load(reprt);
                Integer p = Integer.parseInt(cari.getText());
                //param.clear();
                param.put("idnisn",p);
                JasRep = JasperCompileManager.compileReport(JasDes);
                JasPri = JasperFillManager.fillReport(JasRep, param, conn);
                JasperViewer.viewReport(JasPri, false);
                // JasperPrintManager.PrintReport(JasPri, true);
        } catch (Exception e){
                JOptionPane.showMessageDialog(null, "report salah");
        }
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Salah terus dah pokonya, namanya juga cwo");
        }
    }//GEN-LAST:event_BsadataActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(formsiswatabel3c.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(formsiswatabel3c.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(formsiswatabel3c.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(formsiswatabel3c.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new formsiswatabel3c().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Bsadata;
    private javax.swing.JTextField cari;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbljum;
    private javax.swing.JTable tabelsiswa;
    // End of variables declaration//GEN-END:variables
}

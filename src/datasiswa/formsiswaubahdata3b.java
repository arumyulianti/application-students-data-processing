/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasiswa;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JSpinner.NumberEditor;
import koneksi.koneksi;
import javax.swing.JScrollPane;

/**
 *
 * @author ASUS
 */
public class formsiswaubahdata3b extends javax.swing.JFrame {
private Connection conn = new koneksi().connect();
private DefaultTableModel tabmode;

    /**
     * Creates new form formsiswa
     */
    public formsiswaubahdata3b() {
        initComponents();
        kosong();
        aktif();
        datatable();
        setLocationRelativeTo(this);
    }
    
    protected void aktif(){
    nisn.requestFocus();
    tgl.setSelectedItem(null);
    bulan.setSelectedItem(null);
    tahun.setSelectedItem(null);
    }
     
    protected void kosong(){
    no.setText("");
    nama.setText("");
    buttonGroup1.clearSelection();
    nisn.setText("");
    tempat.setText("");
    tgl.setSelectedItem(null);
    bulan.setSelectedItem(null);
    tahun.setSelectedItem(null);
    usia.setText("");
    bb.setText("");
    tb.setText("");
    nik.setText("");
    kk.setText("");
    agama.setSelectedItem(null);
    alamat.setText("");
    rt.setText("");
    rw.setText("");
    kel.setText("");
    kec.setText("");
    pos.setText("");
    kls.setText("");
    s_asal.setText("");
    mutasi.setText("");
    a_nama.setText("");
    a_nik.setText("");
    a_tl.setText("");
    a_pendidikan.setSelectedItem(null);
    a_pekerjaan.setText("");
    a_penghasilan.setText("");
    a_hp.setText("");
    i_nama.setText("");
    i_nik.setText("");
    i_tl.setText("");
    i_pendidikan.setSelectedItem(null);
    i_pekerjaan.setText("");
    i_penghasilan.setText("");
    i_hp.setText("");    
 }
    public void removeTable(){
        try{
            for (int t=tabmode.getRowCount(); t>0; t--) {tabmode.removeRow(0);}
        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    protected void datatable(){
        removeTable();
        int jumdata=0;
        Object[]Baris={"NIS","Nama","Jenis Kelamin","NISN","Tempat Lahir","Tanggal Lahir","Bulan Lahir", "Tahun Lahir", "Usia", "Berat Badan", "Tinggi Badan", "NIK", "NO KK", "Agama", "Alamat", "RT", "RW","Kelurahan","Kecamatan","Kode Pos","Rombel","Sekolah Asal","Mutasi Ke","Nama Ayah","NIK Ayah","Tahun Lahir","Pendidikan","Pekerjaan","Penghasilan","No Telp","Nama Ibu","NIK Ibu","Tahun Lahir","Pendidikan","Pekerjaan","Penghasilan","No Telp"};
        tabmode = new DefaultTableModel(null,Baris);
        //tabelsiswa.setModel(tabmode);
        String sql = "Select * from datakelas_3b";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
            String nomor = hasil.getString("nomor");
            String nama = hasil.getString("nama");
            String jeniskelamin = hasil.getString("jenis_kelamin");
            String nisn = hasil.getString("nisn");
            String tempat = hasil.getString("tempat");
            String tanggal = hasil.getString("tanggal");
            String bulan = hasil.getString("bulan");
            String tahun = hasil.getString("tahun");
            String usia = hasil.getString("usia");
            String beratbadan = hasil.getString("berat_badan");
            String tinggibadan = hasil.getString("tinggi_badan");
            String nik = hasil.getString("nik");
            String kk = hasil.getString("no_kk");
            String agama = hasil.getString("agama");
            String alamat = hasil.getString("alamat");
            String rt = hasil.getString("rt");
            String rw = hasil.getString("rw");
            String kelurahan = hasil.getString("kelurahan");
            String kecamatan = hasil.getString("kecamatan");
            String kodepos = hasil.getString("kode_pos");
            String rombel = hasil.getString("rombel");
            String sekolahasal = hasil.getString("sekolah_asal");
            String mutasi = hasil.getString("mutasi");
            String namaayah = hasil.getString("nama_ayah");
            String nikayah = hasil.getString("nik_ayah");
            String tahunlahirayah = hasil.getString("tahun_lahir_ayah");
            String jenjangpendidikanayah = hasil.getString("jenjang_pendidikan_ayah");
            String pekerjaanayah = hasil.getString("pekerjaan_ayah");
            String penghasilanayah = hasil.getString("penghasilan_ayah");
            String notlpayah = hasil.getString("no_tlp_ayah");
            String namaibu = hasil.getString("nama_ibu");
            String nikibu = hasil.getString("nik_ibu");
            String tahunlahiribu = hasil.getString("tahun_lahir_ibu");
            String jenjangpendidikanibu = hasil.getString("jenjang_pendidikan_ibu");
            String pekerjaanibu = hasil.getString("pekerjaan_ibu");
            String penghasilanibu = hasil.getString("penghasilan_ibu");
            String notlpibu = hasil.getString("no_tlp_ibu");
            
            String[] data = {nomor,nama,jeniskelamin,nisn,tempat,tanggal,bulan,tahun,usia,beratbadan,tinggibadan,nik,kk,agama,alamat,rt,rw,kelurahan,kecamatan,kodepos,rombel,sekolahasal,mutasi,namaayah,nikayah,tahunlahirayah,jenjangpendidikanayah,pekerjaanayah,penghasilanayah,notlpayah,namaibu,nikibu,tahunlahiribu,jenjangpendidikanibu,pekerjaanibu,penghasilanibu,notlpibu};
            tabmode.addRow(data);
            jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        lbljum.setText(jumdata + " Siswa");
        }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel41 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        a_nama = new javax.swing.JTextField();
        a_nik = new javax.swing.JTextField();
        a_tl = new javax.swing.JTextField();
        a_pekerjaan = new javax.swing.JTextField();
        a_penghasilan = new javax.swing.JTextField();
        a_hp = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        i_nama = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        i_nik = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        i_tl = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        i_pekerjaan = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        i_penghasilan = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        i_hp = new javax.swing.JTextField();
        i_pendidikan = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        a_pendidikan = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        ubah = new java.awt.Button();
        hapus = new java.awt.Button();
        jLabel37 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        lbljum = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        no = new javax.swing.JTextField();
        nama = new javax.swing.JTextField();
        nisn = new javax.swing.JTextField();
        tempat = new javax.swing.JTextField();
        usia = new javax.swing.JTextField();
        bb = new javax.swing.JTextField();
        tb = new javax.swing.JTextField();
        nik = new javax.swing.JTextField();
        kk = new javax.swing.JTextField();
        rt = new javax.swing.JTextField();
        rw = new javax.swing.JTextField();
        kel = new javax.swing.JTextField();
        kec = new javax.swing.JTextField();
        pos = new javax.swing.JTextField();
        tgl = new javax.swing.JComboBox();
        bulan = new javax.swing.JComboBox();
        jLabel22 = new javax.swing.JLabel();
        tahun = new javax.swing.JComboBox();
        radiopr = new javax.swing.JRadioButton();
        radiolaki = new javax.swing.JRadioButton();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        kls = new javax.swing.JTextField();
        s_asal = new javax.swing.JTextField();
        mutasi = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        alamat = new javax.swing.JTextArea();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        agama = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(153, 204, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("DATA SISWA SDN GEDONG 10 PAGI");
        getContentPane().add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(348, 0, -1, -1));

        jPanel6.setBackground(new java.awt.Color(0, 0, 0));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 204, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 0, 0), 4));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel23.setText("Nama");

        jLabel24.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel24.setText("NIK");

        jLabel25.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel25.setText("Tahun Lahir");

        jLabel26.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel26.setText("Jenjang Pendidikan");

        jLabel27.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel27.setText("Pekerjaan");

        jLabel28.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel28.setText("Penghasilan");

        jLabel29.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel29.setText("No Handphone");

        a_nama.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        a_nama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a_namaActionPerformed(evt);
            }
        });

        a_nik.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        a_nik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a_nikActionPerformed(evt);
            }
        });

        a_tl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        a_tl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a_tlActionPerformed(evt);
            }
        });

        a_pekerjaan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        a_pekerjaan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a_pekerjaanActionPerformed(evt);
            }
        });

        a_penghasilan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        a_hp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        a_hp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a_hpActionPerformed(evt);
            }
        });

        jLabel45.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel45.setText("Nama");

        i_nama.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        i_nama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_namaActionPerformed(evt);
            }
        });

        jLabel46.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel46.setText("NIK");

        i_nik.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        i_nik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_nikActionPerformed(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel47.setText("Tahun Lahir");

        i_tl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel48.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel48.setText("Jenjang Pendidikan");

        jLabel49.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel49.setText("Pekerjaan");

        i_pekerjaan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel50.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel50.setText("Penghasilan");

        i_penghasilan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel51.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel51.setText("No Handphone");

        i_hp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        i_pendidikan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        i_pendidikan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TK", "Putus SD", "SD/Sederajat", "SMP/Sederajat", "SMA/Sederajat", "D1", "D2", "D3", "S1", "S2", "S3", "Paket A", "Paket B", "Paket C" }));
        i_pendidikan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i_pendidikanActionPerformed(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(204, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Data Ayah");

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setText("Data Ibu");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(259, 259, 259)
                .addComponent(jLabel33)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel33))
                .addContainerGap())
        );

        a_pendidikan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        a_pendidikan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TK", "Putus SD", "SD/Sederajat", "SMP/Sederajat", "SMA/Sederajat", "D1", "D2", "D3", "S1", "S2", "S3", "Paket A", "Paket B", "Paket C", " ", " " }));
        a_pendidikan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a_pendidikanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26)
                    .addComponent(jLabel27)
                    .addComponent(jLabel28)
                    .addComponent(jLabel29)
                    .addComponent(jLabel25)
                    .addComponent(jLabel24)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(a_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(a_nik, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(a_tl, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(a_hp, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(a_penghasilan, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(a_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(a_pendidikan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46)
                    .addComponent(jLabel45)
                    .addComponent(jLabel47)
                    .addComponent(jLabel48)
                    .addComponent(jLabel49)
                    .addComponent(jLabel50)
                    .addComponent(jLabel51))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(i_nik, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(i_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(i_tl, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(i_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(i_pendidikan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(i_penghasilan, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(i_hp, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(32, Short.MAX_VALUE))
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(jLabel45)
                    .addComponent(i_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_nik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel46)
                    .addComponent(i_nik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_tl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25)
                    .addComponent(jLabel47)
                    .addComponent(i_tl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_pendidikan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26)
                    .addComponent(jLabel48)
                    .addComponent(i_pendidikan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(jLabel49)
                    .addComponent(i_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_penghasilan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(jLabel50)
                    .addComponent(i_penghasilan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel51)
                    .addComponent(i_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(72, 72, 72))
        );

        jPanel6.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 190, -1, 296));

        jPanel3.setBackground(new java.awt.Color(255, 204, 102));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 0, 0), 4));
        jPanel3.setForeground(new java.awt.Color(255, 255, 255));

        ubah.setBackground(new java.awt.Color(0, 153, 0));
        ubah.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        ubah.setLabel("UBAH");
        ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ubahActionPerformed(evt);
            }
        });

        hapus.setBackground(new java.awt.Color(0, 51, 255));
        hapus.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        hapus.setLabel("HAPUS");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/24 Pixel/008-edit-1.png"))); // NOI18N

        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/24 Pixel/010-delete.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel37)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addComponent(jLabel52)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(hapus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(ubah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 520, -1, -1));

        lbljum.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbljum.setForeground(new java.awt.Color(255, 255, 255));
        lbljum.setText("jLabel30");
        jPanel6.add(lbljum, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 610, -1, -1));

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setText("JUMLAH  DATA SISWA KELAS 3B :");
        jPanel6.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 610, -1, -1));

        jPanel1.setBackground(new java.awt.Color(255, 204, 102));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 0, 0), 4));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setText("NIS");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setText("Nama");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setText("Jenis Kelamin");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setText("NISN");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel6.setText("Tempat Tanggal Lahir");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText("Tempat");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel8.setText("Tanggal");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel9.setText("Tahun");

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel10.setText("Usia");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel11.setText("Berat Badan");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel12.setText("Tinggi Badan");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel13.setText("NIK");

        jLabel14.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel14.setText("No KK");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel15.setText("Agama");

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel16.setText("Alamat");

        jLabel17.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel17.setText("RT");

        jLabel18.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel18.setText("RW");

        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel19.setText("Kelurahan");

        jLabel20.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel20.setText("Kecamatan");

        jLabel21.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel21.setText("Kode Pos");

        no.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        nama.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        nisn.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        nisn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nisnActionPerformed(evt);
            }
        });

        tempat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tempat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tempatActionPerformed(evt);
            }
        });

        usia.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        bb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbActionPerformed(evt);
            }
        });

        tb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        nik.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        kk.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        rt.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        rw.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        kel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        kel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kelActionPerformed(evt);
            }
        });

        kec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        tgl.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tgl.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        bulan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bulan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" }));

        jLabel22.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel22.setText("Bulan");

        tahun.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        tahun.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030" }));

        radiopr.setBackground(new java.awt.Color(255, 204, 102));
        buttonGroup1.add(radiopr);
        radiopr.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        radiopr.setText("Perempuan");

        radiolaki.setBackground(new java.awt.Color(255, 204, 102));
        buttonGroup1.add(radiolaki);
        radiolaki.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        radiolaki.setText("Laki-Laki");

        jLabel38.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel38.setText("Rombel Saat Ini");

        jLabel39.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel39.setText("Sekolah Asal");

        jLabel40.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel40.setText("Mutasi Ke");

        kls.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        s_asal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        mutasi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mutasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mutasiActionPerformed(evt);
            }
        });

        alamat.setColumns(20);
        alamat.setRows(5);
        jScrollPane1.setViewportView(alamat);

        jLabel42.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel42.setText("Kg");

        jLabel43.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel43.setText("Cm");

        agama.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        agama.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Islam", "Kristen", "Budha", "Hindu", "Konghucu" }));

        jPanel4.setBackground(new java.awt.Color(204, 0, 0));

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setText("Data Siswa");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel32)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel32)
                .addContainerGap())
        );

        jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/user-female-skin-type-7.png"))); // NOI18N

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/user-male-skin-type-7.png"))); // NOI18N

        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/CARI BENER.png"))); // NOI18N
        jLabel44.setToolTipText("CARI");
        jLabel44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel44MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel6)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel14)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel22)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bulan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(bb, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tb, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel42)
                                    .addComponent(jLabel43)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(usia, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                                .addComponent(tempat, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(no, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                                .addComponent(nama, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(nik, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(nisn, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel54)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiopr)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel30)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(radiolaki))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel44)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel15)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel40)
                            .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel39))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rw, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rt, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(s_asal, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                            .addComponent(kls)
                            .addComponent(pos)
                            .addComponent(kec)
                            .addComponent(kel)
                            .addComponent(mutasi)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(24, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(kk, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(no, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel16)
                            .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radiolaki)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(radiopr))
                            .addComponent(jLabel54)
                            .addComponent(jLabel30))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(nisn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6))
                            .addComponent(jLabel44)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tempat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel17)
                    .addComponent(rt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bulan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(rw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(kel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(kec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel10))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(usia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(bb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel42)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel43)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(kk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(kls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel38))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(s_asal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel39))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mutasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel40))))
                .addContainerGap(64, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 690, 510));

        jLabel34.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setText("Jl. H. Taiman Rt 005 / Rw 010 Kelurahan Gedong, Kecamatan Pasar Rebo");
        jPanel6.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, -1, -1));

        jLabel35.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setText("Jakarta Timur, DKI Jakarta");
        jPanel6.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 60, -1, -1));

        jLabel36.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setText("Kode Pos 13760");
        jPanel6.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(606, 83, -1, -1));

        jPanel7.setBackground(new java.awt.Color(204, 0, 0));

        jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/osis sd.png"))); // NOI18N

        jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/tut wuri2.png"))); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel60)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1130, Short.MAX_VALUE)
                .addComponent(jLabel53)
                .addGap(24, 24, 24))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel60, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel6.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 1370, 110));

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/home.png"))); // NOI18N
        jLabel55.setToolTipText("MENU");
        jLabel55.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel55MouseClicked(evt);
            }
        });
        jPanel6.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 120, -1, -1));

        jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/editt.png"))); // NOI18N
        jLabel56.setToolTipText("UBAH DATA");
        jLabel56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel56MouseClicked(evt);
            }
        });
        jPanel6.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 120, -1, 60));

        jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/report-icon.png"))); // NOI18N
        jLabel57.setToolTipText("LIHAT DATA");
        jLabel57.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel57MouseClicked(evt);
            }
        });
        jPanel6.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 120, -1, -1));

        jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/kembali.png"))); // NOI18N
        jLabel58.setToolTipText("KELUAR");
        jLabel58.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel58MouseClicked(evt);
            }
        });
        jPanel6.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 120, -1, -1));

        getContentPane().add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, 0, 1380, 720));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel58MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel58MouseClicked
        new login().setVisible(true);
        dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel58MouseClicked

    private void jLabel57MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel57MouseClicked
        new formsiswatabel3b().setVisible(true);
        dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel57MouseClicked

    private void jLabel56MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel56MouseClicked
        new formsiswaubahdata3b().setVisible(true);
        dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel56MouseClicked

    private void jLabel55MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel55MouseClicked
        new pilihmenu3b().setVisible(true);
        dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel55MouseClicked

    private void mutasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mutasiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mutasiActionPerformed

    private void bbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bbActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bbActionPerformed

    private void tempatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tempatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tempatActionPerformed

    private void nisnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nisnActionPerformed

        try
        {
            String sql="select * from datakelas_3b where nisn='"+nisn.getText()+"'";
            Statement stat = conn.createStatement();
            ResultSet rs=stat.executeQuery(sql);

            while(rs.next())
            {
                no.setText(rs.getString("nomor"));
                nama.setText(rs.getString("nama"));
                if(rs.getString("jenis_kelamin").equals("Laki-Laki")){
                    radiolaki.setSelected(true);
                }
                else if(rs.getString("jenis_kelamin").equals("Perempuan")){
                    radiolaki.setSelected(true);
                }
                else{}
                nisn.setText(rs.getString("nisn"));
                tempat.setText(rs.getString("tempat"));
                tgl.setSelectedItem(rs.getString("tanggal"));
                bulan.setSelectedItem(rs.getString("bulan"));
                tahun.setSelectedItem(rs.getString("tahun"));
                usia.setText(rs.getString("usia"));
                bb.setText(rs.getString("berat_badan"));
                tb.setText(rs.getString("tinggi_badan"));
                nik.setText(rs.getString("nik"));
                kk.setText(rs.getString("no_kk"));
                agama.setSelectedItem(rs.getString("agama"));
                alamat.setText(rs.getString("alamat"));
                rt.setText(rs.getString("rt"));
                rw.setText(rs.getString("rw"));
                kel.setText(rs.getString("kelurahan"));
                kec.setText(rs.getString("kecamatan"));
                pos.setText(rs.getString("kode_pos"));
                kls.setText(rs.getString("rombel"));
                s_asal.setText(rs.getString("sekolah_asal"));
                mutasi.setText(rs.getString("mutasi"));
                a_nama.setText(rs.getString("nama_ayah"));
                a_nik.setText(rs.getString("nik_ayah"));
                a_tl.setText(rs.getString("tahun_lahir_ayah"));
                a_pendidikan.setSelectedItem(rs.getString("jenjang_pendidikan_ayah"));
                a_pekerjaan.setText(rs.getString("pekerjaan_ayah"));
                a_penghasilan.setText(rs.getString("penghasilan_ayah"));
                a_hp.setText(rs.getString("no_tlp_ayah"));
                i_nama.setText(rs.getString("nama_ibu"));
                i_nik.setText(rs.getString("nik_ibu"));
                i_tl.setText(rs.getString("tahun_lahir_ibu"));
                i_pendidikan.setSelectedItem(rs.getString("jenjang_pendidikan_ibu"));
                i_pekerjaan.setText(rs.getString("pekerjaan_ibu"));
                i_penghasilan.setText(rs.getString("penghasilan_ibu"));
                i_hp.setText(rs.getString("no_tlp_ibu"));
            }
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Salah Memasukan NISN");
        }

        // TODO add your handling code here:
        // TODO add your handling code here:
    }//GEN-LAST:event_nisnActionPerformed

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
        int ok = JOptionPane.showConfirmDialog(null,"Hapus","Konfirmasi Dialog",JOptionPane.YES_NO_OPTION);
        if (ok==0){
            String sql = "delete from datakelas_3b where nisn='"+nisn.getText()+"'";
            try{
                PreparedStatement stat = conn.prepareStatement(sql);
                stat.executeUpdate();
                JOptionPane.showMessageDialog(null,"Data Berhasil Dihapus");
                kosong();
                nisn.requestFocus();
                datatable();
            }
            catch (SQLException e){
                JOptionPane.showMessageDialog(null,"Data Gagal Dihapus"+e);
            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_hapusActionPerformed

    private void ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ubahActionPerformed
        String jenis = null;
        if (radiolaki.isSelected()){
            jenis = "Laki-Laki";
        }
        else if(radiopr.isSelected()){
            jenis="Perempuan";
        }
        try{
            String sql = "update datakelas_3b set nomor=?, nama=?, jenis_kelamin=?, nisn=?, tempat=?, tanggal=?, bulan=?, tahun=?, usia=?, berat_badan=?, tinggi_badan=?, nik=?, no_kk=?, agama=?, alamat=?, rt=?, rw=?, kelurahan=?, kecamatan=?, kode_pos=?, rombel=?, sekolah_asal=?, mutasi=?, nama_ayah=?, nik_ayah=?, tahun_lahir_ayah=?, jenjang_pendidikan_ayah=?, pekerjaan_ayah=?, penghasilan_ayah=?, no_tlp_ayah=?, nama_ibu=?, nik_ibu=?, tahun_lahir_ibu=?, jenjang_pendidikan_ibu=?, pekerjaan_ibu=?, penghasilan_ibu=?, no_tlp_ibu=? where nisn='"+nisn.getText()+"'";
            PreparedStatement stat = conn.prepareStatement(sql);

            stat.setString (1, no.getText());
            stat.setString (2, nama.getText());
            stat.setString (3, jenis);
            stat.setString (4, nisn.getText());
            stat.setString (5, tempat.getText());
            stat.setString (6, tgl.getSelectedItem().toString());
            stat.setString (7, bulan.getSelectedItem().toString());
            stat.setString (8, tahun.getSelectedItem().toString());
            stat.setString (9, usia.getText());
            stat.setString (10, bb.getText());
            stat.setString (11, tb.getText());
            stat.setString (12, nik.getText());
            stat.setString (13, kk.getText());
            stat.setString (14, agama.getSelectedItem().toString());
            stat.setString (15, alamat.getText());
            stat.setString (16, rt.getText());
            stat.setString (17, rw.getText());
            stat.setString (18, kel.getText());
            stat.setString (19, kec.getText());
            stat.setString (20, pos.getText());
            stat.setString (21, kls.getText());
            stat.setString (22, s_asal.getText());
            stat.setString (23, mutasi.getText());
            stat.setString (24, a_nama.getText());
            stat.setString (25, a_nik.getText());
            stat.setString (26, a_tl.getText());
            stat.setString (27, a_pendidikan.getSelectedItem().toString());
            stat.setString (28, a_pekerjaan.getText());
            stat.setString (29, a_penghasilan.getText());
            stat.setString (30, a_hp.getText());
            stat.setString (31, i_nama.getText());
            stat.setString (32, i_nik.getText());
            stat.setString (33, i_tl.getText());
            stat.setString (34, i_pendidikan.getSelectedItem().toString());
            stat.setString (35, i_pekerjaan.getText());
            stat.setString (36, i_penghasilan.getText());
            stat.setString (37, i_hp.getText());

            stat.executeUpdate();
            JOptionPane.showMessageDialog(null,"Data Berhasil Diubah");
            kosong();
            no.requestFocus();
        }
        catch (SQLException e){
            JOptionPane.showMessageDialog(null,"Data Gagal Diubah"+e);
        }
        datatable();
        // TODO add your handling code here:

        // TODO add your handling code here:
    }//GEN-LAST:event_ubahActionPerformed

    private void a_pendidikanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a_pendidikanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_a_pendidikanActionPerformed

    private void i_pendidikanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_pendidikanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i_pendidikanActionPerformed

    private void i_nikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_nikActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i_nikActionPerformed

    private void i_namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i_namaActionPerformed

    private void a_hpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a_hpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_a_hpActionPerformed

    private void a_pekerjaanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a_pekerjaanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_a_pekerjaanActionPerformed

    private void a_tlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a_tlActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_a_tlActionPerformed

    private void a_nikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a_nikActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_a_nikActionPerformed

    private void a_namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_a_namaActionPerformed

    private void kelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kelActionPerformed

    private void jLabel44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel44MouseClicked
     try
        {
            String sql="select * from datakelas_3b where nisn='"+nisn.getText()+"'";
            Statement stat = conn.createStatement();
            ResultSet rs=stat.executeQuery(sql);

            while(rs.next())
            {
                no.setText(rs.getString("nomor"));
                nama.setText(rs.getString("nama"));
                if(rs.getString("jenis_kelamin").equals("Laki-Laki")){
                    radiolaki.setSelected(true);
                }
                else if(rs.getString("jenis_kelamin").equals("Perempuan")){
                    radiolaki.setSelected(true);
                }
                else{}
                nisn.setText(rs.getString("nisn"));
                tempat.setText(rs.getString("tempat"));
                tgl.setSelectedItem(rs.getString("tanggal"));
                bulan.setSelectedItem(rs.getString("bulan"));
                tahun.setSelectedItem(rs.getString("tahun"));
                usia.setText(rs.getString("usia"));
                bb.setText(rs.getString("berat_badan"));
                tb.setText(rs.getString("tinggi_badan"));
                nik.setText(rs.getString("nik"));
                kk.setText(rs.getString("no_kk"));
                agama.setSelectedItem(rs.getString("agama"));
                alamat.setText(rs.getString("alamat"));
                rt.setText(rs.getString("rt"));
                rw.setText(rs.getString("rw"));
                kel.setText(rs.getString("kelurahan"));
                kec.setText(rs.getString("kecamatan"));
                pos.setText(rs.getString("kode_pos"));
                kls.setText(rs.getString("rombel"));
                s_asal.setText(rs.getString("sekolah_asal"));
                mutasi.setText(rs.getString("mutasi"));
                a_nama.setText(rs.getString("nama_ayah"));
                a_nik.setText(rs.getString("nik_ayah"));
                a_tl.setText(rs.getString("tahun_lahir_ayah"));
                a_pendidikan.setSelectedItem(rs.getString("jenjang_pendidikan_ayah"));
                a_pekerjaan.setText(rs.getString("pekerjaan_ayah"));
                a_penghasilan.setText(rs.getString("penghasilan_ayah"));
                a_hp.setText(rs.getString("no_tlp_ayah"));
                i_nama.setText(rs.getString("nama_ibu"));
                i_nik.setText(rs.getString("nik_ibu"));
                i_tl.setText(rs.getString("tahun_lahir_ibu"));
                i_pendidikan.setSelectedItem(rs.getString("jenjang_pendidikan_ibu"));
                i_pekerjaan.setText(rs.getString("pekerjaan_ibu"));
                i_penghasilan.setText(rs.getString("penghasilan_ibu"));
                i_hp.setText(rs.getString("no_tlp_ibu"));
            }
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Salah Memasukan NISN");
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel44MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(formsiswaubahdata3b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(formsiswaubahdata3b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(formsiswaubahdata3b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(formsiswaubahdata3b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new formsiswaubahdata3b().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField a_hp;
    private javax.swing.JTextField a_nama;
    private javax.swing.JTextField a_nik;
    private javax.swing.JTextField a_pekerjaan;
    private javax.swing.JComboBox a_pendidikan;
    private javax.swing.JTextField a_penghasilan;
    private javax.swing.JTextField a_tl;
    private javax.swing.JComboBox agama;
    private javax.swing.JTextArea alamat;
    private javax.swing.JTextField bb;
    private javax.swing.JComboBox bulan;
    private javax.swing.ButtonGroup buttonGroup1;
    private java.awt.Button hapus;
    private javax.swing.JTextField i_hp;
    private javax.swing.JTextField i_nama;
    private javax.swing.JTextField i_nik;
    private javax.swing.JTextField i_pekerjaan;
    private javax.swing.JComboBox i_pendidikan;
    private javax.swing.JTextField i_penghasilan;
    private javax.swing.JTextField i_tl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField kec;
    private javax.swing.JTextField kel;
    private javax.swing.JTextField kk;
    private javax.swing.JTextField kls;
    private javax.swing.JLabel lbljum;
    private javax.swing.JTextField mutasi;
    private javax.swing.JTextField nama;
    private javax.swing.JTextField nik;
    private javax.swing.JTextField nisn;
    private javax.swing.JTextField no;
    private javax.swing.JTextField pos;
    private javax.swing.JRadioButton radiolaki;
    private javax.swing.JRadioButton radiopr;
    private javax.swing.JTextField rt;
    private javax.swing.JTextField rw;
    private javax.swing.JTextField s_asal;
    private javax.swing.JComboBox tahun;
    private javax.swing.JTextField tb;
    private javax.swing.JTextField tempat;
    private javax.swing.JComboBox tgl;
    private java.awt.Button ubah;
    private javax.swing.JTextField usia;
    // End of variables declaration//GEN-END:variables
}

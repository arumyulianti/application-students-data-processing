/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasiswa;
import java.awt.Frame;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JSpinner.NumberEditor;
import koneksi.koneksi;
import javax.swing.*;
import javax.swing.table.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


/**
 *
 * @author ASUS
 */
public class formnilai4b extends javax.swing.JFrame {
private Connection conn = new koneksi().connect();
private DefaultTableModel tabmode;
JasperDesign JasDes;
JasperReport JasRep;
JasperPrint JasPri;
Map param = new HashMap();

    /**
     * Creates new form formsiswa
     */
    public formnilai4b() {
        initComponents();
        kosong();
        aktif();
        tampilmodel();
        datatable();
        setLocationRelativeTo(this);
    }
    
    protected void aktif(){
    nis.requestFocus();
    }
     
    protected void kosong(){
    buttonGroup1.clearSelection();
    agama.setText("");
    nama.setText("");
    nis.setText("");
    nisn.setText("");
    kelas.setText("");
    mtk.setText("");
    bindo.setText("");
    ipa.setText("");
    ips.setText("");
    pkn.setText("");        
    binggris.setText("");        
    seni.setText("");
    pjok.setText("");
    txttotal.setText("");
 }
    public void removeTable(){
        try{
            for (int t=tabmode.getRowCount(); t>0; t--) {tabmode.removeRow(0);}
        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
  
    private void atur(JTable lihat,  int lebar[]){
    try{
        lihat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int banyak = lihat.getColumnCount();
        for (int i = 0; i > banyak; i++) {
           TableColumn kolom = lihat.getColumnModel().getColumn(i);
           kolom.setPreferredWidth(lebar[i]);
           lihat.setRowHeight(20);
        }
    } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "salah"+e);
    }
}
    
    private void tampilmodel(){
    try {
   String[]kolom={"NIS","NISN","NAMA","JENIS KELAMIN","KELAS","MATEMATIKA","BAHASA INDONESIA","IPA","IPS","PKN","AGAMA","BAHASA INGGRIS","SENI BUDAYA","PJOK","RATA RATA"};
        
   DefaultTableModel dtm = new DefaultTableModel(null, kolom){            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };

         jTable1.setModel(dtm);
            atur(jTable1, new int []{100,300,300,90,90,90,90} );
       } catch (Exception e) {
         JOptionPane.showMessageDialog(null, "salah"+e);
    }
}
    
    
    protected void datatable(){
        removeTable();
        int jumdata=0;
        Object[]Baris={"NIS","NISN","NAMA","JENIS KELAMIN","KELAS","MATEMATIKA","BAHASA INDONESIA","IPA","IPS","PKN","AGAMA","BAHASA INGGRIS","SENI BUDAYA","PJOK","RATA RATA"};
        tabmode = new DefaultTableModel(null,Baris);
        jTable1.setModel(tabmode);
        String sql = "Select * from datanilai_4b";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while(hasil.next()){
            String nis = hasil.getString("nis");
            String nisn = hasil.getString("nisn");
            String nama = hasil.getString("nama");
            String jkel = hasil.getString("jkel");
            String kelas = hasil.getString("kelas");
            String mtk = hasil.getString("mtk");
            String bindo = hasil.getString("bindo");
            String ipa = hasil.getString("ipa");
            String ips = hasil.getString("ips");
            String pkn = hasil.getString("pkn");
            String agama = hasil.getString("agama");
            String binggris = hasil.getString("binggris");
            String senibudaya = hasil.getString("senibudaya");
            String pjok = hasil.getString("pjok");
            String rata = hasil.getString("rata");
            
            String[] data = {nis,nisn,nama,jkel,kelas,mtk,bindo,ipa,ips,pkn,agama,binggris,senibudaya,pjok,rata};
            tabmode.addRow(data);
            jumdata = jumdata+1;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        jLabel2.setText(jumdata + " Siswa");
        }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel41 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        nama = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        nis = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        radiopr = new javax.swing.JRadioButton();
        jLabel30 = new javax.swing.JLabel();
        radiolaki = new javax.swing.JRadioButton();
        kelas = new javax.swing.JTextField();
        ubah = new java.awt.Button();
        hapus = new java.awt.Button();
        reset = new java.awt.Button();
        simpan = new java.awt.Button();
        jLabel50 = new javax.swing.JLabel();
        nisn = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        mtk = new javax.swing.JTextField();
        bindo = new javax.swing.JTextField();
        ipa = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        pkn = new javax.swing.JTextField();
        jLabel62 = new javax.swing.JLabel();
        ips = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        agama = new javax.swing.JTextField();
        jLabel64 = new javax.swing.JLabel();
        binggris = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        seni = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        pjok = new javax.swing.JTextField();
        txttotal = new javax.swing.JTextField();
        jLabel67 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        cari = new java.awt.Button();
        txtcari = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cetak = new java.awt.Button();
        jLabel1 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(153, 204, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("DATA SISWA SDN GEDONG 10 PAGI");
        getContentPane().add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(348, 0, -1, -1));

        jPanel6.setBackground(new java.awt.Color(0, 0, 0));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 204, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 0, 0), 4));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jLabel45.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel45.setText("Nama");

        nama.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        nama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaActionPerformed(evt);
            }
        });

        jLabel46.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel46.setText("NIS");

        nis.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        nis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nisActionPerformed(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(204, 0, 0));

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setText("Data Nilai");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel33)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jLabel33)
                .addContainerGap())
        );

        jLabel47.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel47.setText("Jenis Kelamin");

        jLabel48.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel48.setText("Kelas");

        jLabel49.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel49.setText("Data Nilai");

        jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/user-female-skin-type-7.png"))); // NOI18N

        radiopr.setBackground(new java.awt.Color(255, 204, 102));
        buttonGroup1.add(radiopr);
        radiopr.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        radiopr.setText("Perempuan");

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/user-male-skin-type-7.png"))); // NOI18N

        radiolaki.setBackground(new java.awt.Color(255, 204, 102));
        buttonGroup1.add(radiolaki);
        radiolaki.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        radiolaki.setText("Laki-Laki");

        ubah.setBackground(new java.awt.Color(0, 153, 0));
        ubah.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        ubah.setLabel("UBAH");
        ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ubahActionPerformed(evt);
            }
        });

        hapus.setBackground(new java.awt.Color(0, 153, 0));
        hapus.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        hapus.setLabel("HAPUS");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        reset.setBackground(new java.awt.Color(0, 153, 0));
        reset.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        reset.setLabel("RESET");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });

        simpan.setBackground(new java.awt.Color(0, 153, 0));
        simpan.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        simpan.setLabel("SIMPAN");
        simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanActionPerformed(evt);
            }
        });

        jLabel50.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel50.setText("NISN");

        nisn.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        nisn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nisnActionPerformed(evt);
            }
        });

        jLabel51.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel51.setText("Matematika");

        jLabel52.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel52.setText("B. Indonesia");

        jLabel60.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel60.setText("IPA");

        mtk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                mtkKeyReleased(evt);
            }
        });

        bindo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bindoKeyReleased(evt);
            }
        });

        ipa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ipaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ipaKeyReleased(evt);
            }
        });

        jLabel61.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel61.setText("IPS");

        pkn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pknKeyReleased(evt);
            }
        });

        jLabel62.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel62.setText("PKN");

        ips.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ipsActionPerformed(evt);
            }
        });
        ips.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ipsKeyReleased(evt);
            }
        });

        jLabel63.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel63.setText("Agama");

        agama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                agamaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                agamaKeyReleased(evt);
            }
        });

        jLabel64.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel64.setText("B. Inggris");

        binggris.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                binggrisKeyReleased(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel65.setText("Seni Budaya");

        seni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                seniKeyReleased(evt);
            }
        });

        jLabel66.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel66.setText("PJOK");

        pjok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pjokActionPerformed(evt);
            }
        });
        pjok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pjokKeyReleased(evt);
            }
        });

        txttotal.setBackground(new java.awt.Color(204, 0, 0));
        txttotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttotalActionPerformed(evt);
            }
        });
        txttotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttotalKeyPressed(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel67.setText("Nilai Rata-rata");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel45)
                                    .addComponent(jLabel46)
                                    .addComponent(jLabel47)
                                    .addComponent(jLabel48)
                                    .addComponent(jLabel49))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nis, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nisn, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel54)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiopr)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel30)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiolaki))
                                    .addComponent(kelas, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(pkn, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(ipa, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(bindo, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(mtk, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(ips, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)))
                                        .addGap(27, 27, 27)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel64)
                                            .addComponent(jLabel63)
                                            .addComponent(jLabel65)
                                            .addComponent(jLabel66)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel67)
                                                .addGap(55, 55, 55)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(seni, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(binggris, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(agama, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(pjok, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                            .addComponent(jLabel50)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel52)
                            .addComponent(jLabel51)
                            .addComponent(jLabel60)
                            .addComponent(jLabel61)
                            .addComponent(jLabel62))))
                .addContainerGap(34, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(simpan, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reset, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel46)
                            .addComponent(nis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel50)
                            .addComponent(nisn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel45)
                            .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radiolaki)
                            .addComponent(jLabel30)
                            .addComponent(radiopr)
                            .addComponent(jLabel54)
                            .addComponent(jLabel47, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel48)
                            .addComponent(kelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel49)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel51)
                            .addComponent(mtk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel52)
                            .addComponent(bindo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel60)
                            .addComponent(ipa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel61)
                            .addComponent(ips, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel63)
                            .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel64)
                            .addComponent(binggris, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel65)
                            .addComponent(seni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel66)
                            .addComponent(pjok, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel62)
                    .addComponent(pkn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel67))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(reset, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(hapus, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(ubah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(simpan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(28, 28, 28))
        );

        jPanel6.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 450, 500));

        jPanel3.setBackground(new java.awt.Color(255, 204, 102));
        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 0, 0), 4, true));
        jPanel3.setForeground(new java.awt.Color(255, 255, 255));

        jPanel8.setBackground(new java.awt.Color(204, 0, 0));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        cari.setBackground(new java.awt.Color(0, 153, 0));
        cari.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        cari.setLabel("CARI");
        cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cariActionPerformed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setText("JUMLAH  DATA SISWA :");

        jLabel2.setText("jLabel2");

        cetak.setBackground(new java.awt.Color(0, 153, 0));
        cetak.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        cetak.setLabel("REPORT DATA");
        cetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cetakActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel1.setText("NIS");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addGap(117, 117, 117))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addComponent(cetak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtcari, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(28, 28, 28))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(31, 31, 31)))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cari, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(txtcari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(cetak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jLabel2))
                .addGap(19, 19, 19))
        );

        jPanel6.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 190, 810, 490));

        jLabel34.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setText("Jl. H. Taiman Rt 005 / Rw 010 Kelurahan Gedong, Kecamatan Pasar Rebo");
        jPanel6.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, -1, -1));

        jLabel35.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setText("Jakarta Timur, DKI Jakarta");
        jPanel6.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 60, -1, -1));

        jLabel36.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setText("Kode Pos 13760");
        jPanel6.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(606, 83, -1, -1));

        jPanel7.setBackground(new java.awt.Color(204, 0, 0));

        jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/osis sd.png"))); // NOI18N

        jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/tut wuri2.png"))); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel59)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1151, Short.MAX_VALUE)
                .addComponent(jLabel53)
                .addGap(23, 23, 23))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel53, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel59, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel6.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1370, 110));

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/home.png"))); // NOI18N
        jLabel55.setToolTipText("MENU");
        jLabel55.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel55MouseClicked(evt);
            }
        });
        jPanel6.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 120, -1, -1));

        jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/kembali.png"))); // NOI18N
        jLabel58.setToolTipText("KELUAR");
        jLabel58.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel58MouseClicked(evt);
            }
        });
        jPanel6.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 120, -1, -1));

        getContentPane().add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1380, 710));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel55MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel55MouseClicked
new pilihmenu1a().setVisible(true);
    dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel55MouseClicked

    private void jLabel58MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel58MouseClicked
 new login().setVisible(true);
    dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel58MouseClicked

    private void nisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nisActionPerformed

    private void namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaActionPerformed

    private void ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ubahActionPerformed
    String jenis = null;
    if (radiolaki.isSelected()){
    jenis = "Laki-Laki";
    }
    else if(radiopr.isSelected()){
    jenis="Perempuan";
    }
    try{
        String sql = "update datanilai_4b set nis=?, nisn=?, nama=?, jkel=?, kelas=?, mtk=?, bindo=?, ipa=?, ips=?, pkn=?, agama=?, binggris=?, senibudaya=?, pjok=?, rata=? where nis='"+nis.getText()+"'";
        PreparedStatement stat = conn.prepareStatement(sql);
      
        stat.setString (1, nis.getText());
        stat.setString (2, nisn.getText());
        stat.setString (3, nama.getText()); 
        stat.setString (4, jenis);
        stat.setString (5, kelas.getText());
        stat.setString (6, mtk.getText());
        stat.setString (7, bindo.getText());
        stat.setString (8, ipa.getText());
        stat.setString (9, ips.getText());
        stat.setString (10, pkn.getText());
        stat.setString (11, agama.getText());
        stat.setString (12, binggris.getText());
        stat.setString (13, seni.getText());
        stat.setString (14, pjok.getText());
        stat.setString (15, txttotal.getText());
        
        stat.executeUpdate();
        JOptionPane.showMessageDialog(null,"Data Berhasil Diubah");
        kosong();
        nis.requestFocus();
    }
    catch (SQLException e){
        JOptionPane.showMessageDialog(null,"Data Gagal Diubah"+e);
    }
    datatable();
    }//GEN-LAST:event_ubahActionPerformed

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
    int ok = JOptionPane.showConfirmDialog(null,"Hapus","Konfirmasi Dialog",JOptionPane.YES_NO_OPTION);
    if (ok==0){
        String sql = "delete from datanilai_4b where nis='"+nis.getText()+"'";
        try{
        PreparedStatement stat = conn.prepareStatement(sql);
        stat.executeUpdate();
        JOptionPane.showMessageDialog(null,"Data Berhasil Dihapus");
        kosong();
        nis.requestFocus();
        datatable();
    }
    catch (SQLException e){
        JOptionPane.showMessageDialog(null,"Data Gagal Dihapus"+e);
    }
    }
    }//GEN-LAST:event_hapusActionPerformed

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
    kosong();
    }//GEN-LAST:event_resetActionPerformed

    private void simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanActionPerformed
    String jenis = null;
        if (radiolaki.isSelected()){
            jenis = "Laki-Laki";
        }
        else if(radiopr.isSelected()){
            jenis="Perempuan";
        }
        try {
                String sql = "insert into datanilai_4b values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stat = conn.prepareStatement(sql);
                stat.setString (1, nis.getText());
                stat.setString (2, nisn.getText());
                stat.setString (3, nama.getText()); 
                stat.setString (4, jenis);
                stat.setString (5, kelas.getText());
                stat.setString (6, mtk.getText());
                stat.setString (7, bindo.getText());
                stat.setString (8, ipa.getText());
                stat.setString (9, ips.getText());
                stat.setString (10, pkn.getText());
                stat.setString (11, agama.getText());
                stat.setString (12, binggris.getText());
                stat.setString (13, seni.getText());
                stat.setString (14, pjok.getText());
                stat.setString (15, txttotal.getText());
                
                stat.executeUpdate();
                JOptionPane.showMessageDialog(null, "Data BERHASIL tersimpan" , "Informasi", JOptionPane.INFORMATION_MESSAGE);
             kosong();
             datatable();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Data GAGAL tersimpan" , "Informasi", JOptionPane.INFORMATION_MESSAGE);
            }
    }//GEN-LAST:event_simpanActionPerformed

    private void cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cariActionPerformed
    String itemdicari = txtcari.getText();
    int jumdata=0;
    removeTable();
    try{
        Statement stat = conn.createStatement();
        String sql = "Select * from datanilai_4b where nis like '%"+itemdicari+"%' or nisn like '%"+itemdicari+"%' order by nis asc";
        System.out.println(sql);
        
        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()){
            String nis = rs.getString("nis");
            String nisn = rs.getString("nisn");
            String nama = rs.getString("nama");
            String jkel = rs.getString("jkel");
            String kelas = rs.getString("kelas");
            String mtk = rs.getString("mtk");
            String bindo = rs.getString("bindo");
            String ipa = rs.getString("ipa");
            String ips = rs.getString("ips");
            String pkn = rs.getString("pkn");
            String agama = rs.getString("agama");
            String binggris = rs.getString("binggris");
            String senibudaya  = rs.getString("senibudaya");
            String pjok = rs.getString("pjok");
            String rata = rs.getString("rata");
            
            String[] data = {nis,nisn,nama,jkel,kelas,mtk,bindo,ipa,ips,pkn,agama,binggris,senibudaya,pjok,rata};
            tabmode.addRow(data);
            jumdata = jumdata++;
            }
        }catch (SQLException sqle){
            jumdata=0;
            JOptionPane.showMessageDialog(null,"Data Gagal Masuk Tabel"+sqle);
        }
        jLabel2.setText(jumdata+"Item");
        // TODO add your handling code here:
    }//GEN-LAST:event_cariActionPerformed

    private void nisnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nisnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nisnActionPerformed

    private void ipsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ipsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ipsActionPerformed

    private void pjokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pjokActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pjokActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
    int bar = jTable1.getSelectedRow();
    String a = tabmode.getValueAt(bar, 0).toString();
    String b = tabmode.getValueAt(bar, 1).toString();
    String c = tabmode.getValueAt(bar, 2).toString();
    String d = tabmode.getValueAt(bar, 3).toString();
    String e = tabmode.getValueAt(bar, 4).toString();
    String f = tabmode.getValueAt(bar, 5).toString();
    String g = tabmode.getValueAt(bar, 6).toString();
    String h = tabmode.getValueAt(bar, 7).toString();
    String i = tabmode.getValueAt(bar, 8).toString();
    String j = tabmode.getValueAt(bar, 9).toString();
    String k = tabmode.getValueAt(bar, 10).toString();
    String l = tabmode.getValueAt(bar, 11).toString();
    String m = tabmode.getValueAt(bar, 12).toString();
    String n = tabmode.getValueAt(bar, 13).toString();
    String o = tabmode.getValueAt(bar, 14).toString();
    
    nis.setText(a);
    nisn.setText(b);
    nama.setText(c);
    if("Laki-Laki".equals(d)){
        radiolaki.setSelected(true);
    }
    else{
        radiopr.setSelected(true);
    }
    kelas.setText(e);
    mtk.setText(f);
    bindo.setText(g);
    ipa.setText(h);
    ips.setText(i);
    pkn.setText(j);
    agama.setText(k);
    binggris.setText(l);        
    seni.setText(m);
    pjok.setText(n);
    txttotal.setText(o);
    }//GEN-LAST:event_jTable1MouseClicked

    private void agamaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_agamaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_agamaKeyPressed

    private void txttotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttotalActionPerformed

    private void txttotalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttotalKeyPressed
    
    }//GEN-LAST:event_txttotalKeyPressed

    private void ipaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ipaKeyPressed
        
        // TODO add your handling code here:
    }//GEN-LAST:event_ipaKeyPressed

    private void ipaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ipaKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));
    
        // TODO add your handling code here:
    }//GEN-LAST:event_ipaKeyReleased

    private void pjokKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pjokKeyReleased
        //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_pjokKeyReleased

    private void pknKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pknKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

    }//GEN-LAST:event_pknKeyReleased

    private void ipsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ipsKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_ipsKeyReleased

    private void seniKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_seniKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_seniKeyReleased

    private void binggrisKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_binggrisKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_binggrisKeyReleased

    private void bindoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bindoKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_bindoKeyReleased

    private void agamaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_agamaKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_agamaKeyReleased

    private void mtkKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_mtkKeyReleased
    //    if(mtk.getText().isEmpty()&&bindo.getText().isEmpty()){
//    mtk.setText("0");
//    bindo.setText("0");
//    }
//    else if(ipa.getText().isEmpty()){
//    ipa.setText("0");
//    }
    int a = Integer.parseInt(mtk.getText());
    int b = Integer.parseInt(bindo.getText());
    int c = Integer.parseInt(ipa.getText());
    int d = Integer.parseInt(ips.getText());
    int e = Integer.parseInt(pkn.getText());
    int f = Integer.parseInt(agama.getText());
    int g = Integer.parseInt(binggris.getText());
    int h = Integer.parseInt(seni.getText());
    int i = Integer.parseInt(pjok.getText());

    Integer total,rata;
    total= a+b+c+d+e+f+g+h+i;
    rata= total/9;
    txttotal.setText(String.valueOf(rata));

        // TODO add your handling code here:
    }//GEN-LAST:event_mtkKeyReleased

    private void cetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cetakActionPerformed
    String reportsource = null;
    String reportdest = null;
        try{
            Connection conn = new koneksi().connect();
            reportsource = System.getProperty("user.dir")+"/src/report/reportnilai4b.jrxml";
            reportdest = System.getProperty("user.dir")+"/src/report/reportnilai4b.jasper";
            
            JasRep = JasperCompileManager.compileReport(reportsource);
            JasPri = JasperFillManager.fillReport(JasRep,param,conn);
            JasperViewer.viewReport(JasPri, false);
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null," Report Gagal Di Print"+ex);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_cetakActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(formnilai4b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(formnilai4b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(formnilai4b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(formnilai4b.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new formnilai4b().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField agama;
    private javax.swing.JTextField bindo;
    private javax.swing.JTextField binggris;
    private javax.swing.ButtonGroup buttonGroup1;
    private java.awt.Button cari;
    private java.awt.Button cetak;
    private java.awt.Button hapus;
    private javax.swing.JTextField ipa;
    private javax.swing.JTextField ips;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField kelas;
    private javax.swing.JTextField mtk;
    private javax.swing.JTextField nama;
    private javax.swing.JTextField nis;
    private javax.swing.JTextField nisn;
    private javax.swing.JTextField pjok;
    private javax.swing.JTextField pkn;
    private javax.swing.JRadioButton radiolaki;
    private javax.swing.JRadioButton radiopr;
    private java.awt.Button reset;
    private javax.swing.JTextField seni;
    private java.awt.Button simpan;
    private javax.swing.JTextField txtcari;
    private javax.swing.JTextField txttotal;
    private java.awt.Button ubah;
    // End of variables declaration//GEN-END:variables
}
